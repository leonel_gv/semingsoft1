\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}INTRODUCCION}{2}{}%
\contentsline {subsection}{\numberline {1.1}Proposito}{2}{}%
\contentsline {subsection}{\numberline {1.2}Alcance}{2}{}%
\contentsline {subsection}{\numberline {1.3}Personal Involucrado}{2}{}%
\contentsline {subsection}{\numberline {1.4}Definiciones, acronimos y abreviaturas}{2}{}%
\contentsline {subsection}{\numberline {1.5}Referencias}{2}{}%
\contentsline {subsection}{\numberline {1.6}Resumen}{2}{}%
\contentsline {section}{\numberline {2}DESCRIPCION GENERAL}{3}{}%
\contentsline {subsection}{\numberline {2.1}Perspectiva del producto}{3}{}%
\contentsline {subsection}{\numberline {2.2}Funcionalidad del producto}{3}{}%
\contentsline {subsection}{\numberline {2.3}Caracteristicas de los usuarios}{3}{}%
\contentsline {subsection}{\numberline {2.4}Restricciones}{4}{}%
\contentsline {subsection}{\numberline {2.5}Suposiciones y dependencias}{4}{}%
\contentsline {section}{\numberline {3}REQUISITOS ESPECIFICOS}{4}{}%
\contentsline {subsection}{\numberline {3.1}Requisitos comunes de las interfaces}{4}{}%
\contentsline {subsubsection}{\numberline {3.1.1}Interfaces de usuario}{4}{}%
\contentsline {subsubsection}{\numberline {3.1.2}Interfaces de hardware}{4}{}%
\contentsline {subsubsection}{\numberline {3.1.3}Interfaces de software}{5}{}%
\contentsline {subsubsection}{\numberline {3.1.4}Interfaces de comunicacion}{5}{}%
\contentsline {subsection}{\numberline {3.2}Requerimientos funcionales}{5}{}%
\contentsline {subsubsection}{\numberline {3.2.1}Requisito funcional 1}{5}{}%
\contentsline {subsubsection}{\numberline {3.2.2}Requisito funcional 2}{5}{}%
\contentsline {subsubsection}{\numberline {3.2.3}Requisito funcional 3}{5}{}%
\contentsline {subsubsection}{\numberline {3.2.4}Requisito funcional 4}{6}{}%
\contentsline {subsubsection}{\numberline {3.2.5}Requisito funcional 5}{6}{}%
\contentsline {subsubsection}{\numberline {3.2.6}Requisito funcional 6}{6}{}%
\contentsline {subsubsection}{\numberline {3.2.7}Requisito funcional 7}{7}{}%
\contentsline {subsubsection}{\numberline {3.2.8}Requisito funcional 8}{7}{}%
\contentsline {subsubsection}{\numberline {3.2.9}Requisito funcional 9}{7}{}%
\contentsline {subsubsection}{\numberline {3.2.10}Requisito funcional 10}{7}{}%
\contentsline {subsubsection}{\numberline {3.2.11}Requisito funcional 11}{8}{}%
\contentsline {subsubsection}{\numberline {3.2.12}Requisito funcional 12}{8}{}%
\contentsline {subsection}{\numberline {3.3}Requerimientos no funcionales}{8}{}%
\contentsline {subsubsection}{\numberline {3.3.1}Requisitos de rendimiento}{8}{}%
\contentsline {paragraph}{\numberline {3.3.1.1}Requisito no funcional 1}{8}{}%
\contentsline {subsubsection}{\numberline {3.3.2}Seguridad}{9}{}%
\contentsline {paragraph}{\numberline {3.3.2.1}Requisito no funcional 2}{9}{}%
\contentsline {paragraph}{\numberline {3.3.2.2}Requisito no funcional 3}{9}{}%
\contentsline {subsubsection}{\numberline {3.3.3}Fiabilidad}{9}{}%
\contentsline {paragraph}{\numberline {3.3.3.1}Requisito no funcional 4}{9}{}%
\contentsline {subsubsection}{\numberline {3.3.4}Disponibilidad}{10}{}%
\contentsline {paragraph}{\numberline {3.3.4.1}Requisito no funcional 5}{10}{}%
\contentsline {subsubsection}{\numberline {3.3.5}Mantenibilidad}{10}{}%
\contentsline {paragraph}{\numberline {3.3.5.1}Requisito no funcional 6}{10}{}%
\contentsline {subsubsection}{\numberline {3.3.6}Portabilidad}{10}{}%
\contentsline {paragraph}{\numberline {3.3.6.1}Requisito no funcional 7}{10}{}%
